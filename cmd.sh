#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
  echo " Instructions: "
  echo " -h     --help          show this help"
  echo "        --check         check source code convention before build with cppcheck"
  echo " -b     --compile       compile"
  echo "        --build         compile"
  echo " -c     --clean         clean previous compile"
  echo " -t     --test          automation test"
  echo " -cov       <TOOLCHAIN> run coverage"
  echo " --coverage <TOOLCHAIN> run coverage"
  echo " -prof  --profiling     run profiling"
  echo " -r     --execcute      execcute program"
  echo "        --run           execcute program"
  echo ""
  echo " Example:"
  echo " ./cmd.sh -h"
  echo " ./cmd.sh --help"
  echo " ./cmd.sh --check"
  echo " ./cmd.sh -b"
  echo " ./cmd.sh --compile"
  echo " ./cmd.sh --build"
  echo " ./cmd.sh -t"
  echo " ./cmd.sh --test"
  echo " ./cmd.sh -cov"
  echo " ./cmd.sh --coverage LLVM"
  echo " ./cmd.sh -prof"
  echo " ./cmd.sh --profiling"
  echo " ./cmd.sh -r"
  echo " ./cmd.sh --run"
  echo " ./cmd.sh --execcute"
  echo " ./cmd.sh -i"
  echo " ./cmd.sh --info"
  echo

elif [ "$1" == "" ];
then
  echo "error: show help with './cmd.sh -h'"

elif [ "$1" == "--check" ];
then
  exec cppcheck --enable=all --suppress=missingIncludeSystem --force --inconclusive \
       --check-config --std=posix -I /usr/include/c++/5/ -I inc_cpp/ -I src_cpp/ .

elif [ "$1" == "-b" ] || [ "$1" == "--compile" ] || [ "$1" == "--build" ];
then
  exec make

elif [ "$1" == "-c" ] || [ "$1" == "--clean" ];
then
  exec make clean

elif [ "$1" == "-t" ] || [ "$1" == "--test" ];
then
  exec gdb --command=gdb.script

elif [ "$1" == "-cov" ] || [ "$1" == "--coverage" ];
then
  if [ "$2" == "" ] || [ "$2" == "gcc" ] || [ "$2" == "GCC" ];
  then
    exec gcov -bcdfmnpu build/main.gcno

  elif [ "$2" == "llvm" ] || [ "$2" == "LLVM" ];
  then
    exec llvm-cov gcov -bcflnpu build/main.gcno | c++filt

  fi

elif [ "$1" == "-prof" ] || [ "$1" == "--profiling" ];
then
  exec gprof build/demo.exe

elif [ "$1" == "-r" ] || [ "$1" == "--execcute" ] || [ "$1" == "--run" ];
then
  exec make run

elif [ "$1" == "-i" ] || [ "$1" == "--info" ];
then
  exec make info

fi
