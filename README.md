# Demo automation test a C/C++ program and measure coverage
Demo using GDB for automation test a C/C++ program and measure coverage with GCOV/LLVM COV.

Documentation:

* GDB: [https://sourceware.org/gdb/current/onlinedocs/gdb](https://sourceware.org/gdb/current/onlinedocs/gdb)

* GCOV: [https://gcc.gnu.org/onlinedocs/gcc/Gcov.html](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html)

* LLVM COV: [https://llvm.org/docs/CommandGuide/llvm-cov.html](https://llvm.org/docs/CommandGuide/llvm-cov.html)

### 1. Structure of Directory
```
├── build         => auto generate after build (Note: be delete after clean, only present after build)
├── inc_cpp       => a folder contains header files
├── src_cpp       => a folder contains source files
├── cmd.sh        => shell command contains some of executive directives
├── config.mk     => config build enviroment
├── Makefile      => Makefile
├── gdb.script    => the script contains task definitions used in debugging and testing
├── main.cpp      => main of project
└── README.md     => this file
```

### 2. Using
Show all directives with: ```./cmd.sh -h``` or ```./cmd.sh --help``` and follow guides
