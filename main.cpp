/**
  ******************************************************************************
  * @file    main.cpp
  * @author  Bao Son Le
  * @version 1.0.2
  * @date    28-Apr-2018
  * @brief   Demo in C++, Ansi-style, VT100
  ******************************************************************************
  * UPDATE   HISTORY
  * REV      AUTHOR          DATE         DESCRIPTION OF CHANGE
  * ---      -----------     ---------    ---------------------
  * 0.1.0    Bao Son Le      04-Sep-2017  Create this file
  *
  * 0.2.0    Bao Son Le      07-Sep-2017  Replaced C++ labels by assembly labels
  *
  * 0.3.0    Bao Son Le      07-Sep-2017  Convert DIVISBLE_StateTypedef and
  *                                       INLINE_FUNC_OR_NOT to unsigned integer
  *
  * 0.3.1    Bao Son Le      17-Sep-2017  Clear blank lines
  *
  * 0.3.2    Bao Son Le      18-Sep-2017  Replace "Hello World" string with
  *                                       name of toolchain to be used
  *
  * 0.4.0    Bao Son Le      30-Sep-2017  Self-inflicted and check
  *                                       -Waggregate-return warning option
  *
  * 0.4.1    Bao Son Le      30-Sep-2017  Beautify
  *
  * 0.8.0    Bao Son Le      02-Dec-2017  Edited follow project's template
  *
  * 1.0.2    Bao Son Le      28-Apr-2018  Edited follow project's template
  *
  ******************************************************************************
  * MIT License
  *
  * Copyright (c) 2017-2018 Bao Son Le
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  *
  ******************************************************************************
  * @attention
  *
  * YOU DON'T NEED TO ATTENTION TO THIS SECTION
  *
  * <h2><center>&copy; COPYRIGHT 2017 Bao Son Le </center></h2>
  ******************************************************************************
  */

/**
  ******************************************************************************
  *  INCLUDES HEADER FILES
  ******************************************************************************
  */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "template.h"


/**
  ******************************************************************************
  *  PRIVATE DEFINITIONS - MACROS
  ******************************************************************************
  */

#define COVERAGE()  testDivisible(88, 2);  \
                    testDivisible(88, 3);  \
                    testDivisible(123, 3); \
                    testDivisible(33, 11); \
                    testDivisible(123, 11);

#define OVERWRITTEN_VLD_MACRO OVERWRITTEN_MACRO


/**
  ******************************************************************************
  *  PRIVATE VARIABLES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE STRUCTURES - UNIONS - ENUMERATIONS
  ******************************************************************************
  */

/**
  * @brief : Function is an inline or a non-inline
  *          enumeration
  */
typedef enum {
	__INLINE__ = 0,
	__NON_INLINE__
} INLINE_FUNC_OR_NOT;

/**
  * @brief : Divisible state enumeration
  */
typedef enum {
	__DIVISBLE__ = 0,
	__NOT_DIVISBLE__,
	__OPERAND_1_OUT_OF_RANGE__,
	__OPERAND_2_OUT_OF_RANGE__,
	__ALL_OPERANDS_OUT_OF_RANGE__
} DIVISBLE_StateTypedef;

/**
  * @brief : struct which is used to check
  *          -Waggregate-return warning
  */
typedef struct {
	unsigned int a :  8;
	unsigned int b :  8;
	unsigned int   : 16;
} AGGREGATE_Typedef;


/**
  ******************************************************************************
  *  PRIVATE USER-DEFINED TYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE FUNCTION PROTOYPES
  ******************************************************************************
  */

inline unsigned int inlineToUInt(INLINE_FUNC_OR_NOT) __attribute__((always_inline));
inline unsigned int divisibleToUInt(DIVISBLE_StateTypedef) __attribute__((always_inline));
inline INLINE_FUNC_OR_NOT inlineFunc(const char*) __attribute__((always_inline));

INLINE_FUNC_OR_NOT nonInlineFunc(const char*);
DIVISBLE_StateTypedef testDivisible(unsigned int, unsigned int);
void passAndRetStructure(AGGREGATE_Typedef&);


/**
  ******************************************************************************
  *  MAIN FUNCTION
  ******************************************************************************
  */
int main(void) {
	unsigned int       operd1 = 99;
	unsigned int       operd2 =  3;
	unsigned int   overwitten = OVERWRITTEN_VLD_MACRO;

	AGGREGATE_Typedef     st        = { 0, 0 };
	INLINE_FUNC_OR_NOT    inlf      = __INLINE__;
	DIVISBLE_StateTypedef divisible = __DIVISBLE__;
#ifdef LLVM
	std::string s = std::string("!!!Hello Clang!!!");
#else
	std::string s = std::string("!!!Hello GCC!!!");
#endif

__asm("AFTER_INIT:");
	std::cout << s << std::endl << std::endl;
	std::cout << "sizeof(long int) : " << sizeof(long int) << std::endl;
	std::cout << "overwitten : " << overwitten << std::endl;
	std::cout << std::endl;

	printf("Inline function\n");
	inlf = inlineFunc("This is inline function!!");
	printf("STATUS: %u\n\n", inlineToUInt(inlf));

	printf("Non Inline function\n");
	inlf = nonInlineFunc("This is non-inline function!!");
	printf("STATUS: %u\n\n", inlineToUInt(inlf));

	printf("Checked %u divisible by %u\n", operd1, operd2);
__asm("RUN_DIVISIBLE:");
	divisible = testDivisible(operd1, operd2);
__asm("AFTER_RUN_DIVISIBLE:");
	printf("STATUS: %u\n\n", divisibleToUInt(divisible));

__asm("AGGREGATE_TEST_WARN:");
	std::cout << "Before, " << std::endl;
	std::cout << "The first  element value of struct : " << st.a << std::endl;
	std::cout << "The second element value of struct : " << st.b << std::endl;
	passAndRetStructure(st);
	std::cout << std::endl  << "After, " << std::endl;
	std::cout << "The first  element value of struct : " << st.a << std::endl;
	std::cout << "The second element value of struct : " << st.b << std::endl;

#ifdef CODE_COVERAGE
	COVERAGE();
#endif

	return EXIT_SUCCESS;
}


/**
  ******************************************************************************
  *  EXPORTED FUNCTION DEFINITIONS
  ******************************************************************************
  */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */


/**
  ******************************************************************************
  *  PRIVATE FUNCTION DEFINITIONS
  ******************************************************************************
  */

/**
  * @brief : an inline function, print a string
  *          which is passed into this function.
  *          It's used to compared with a non-
  *          inline function in this program.
  * @param : s  A Pointer point to A Constant Data Memory
  * @return: INLINE_FUNC_OR_NOT
  */
inline INLINE_FUNC_OR_NOT inlineFunc(const char* s) {
	printf("%s\n", s);

	return __INLINE__;
}

/**
  * @brief : an non-inline function, print a string
  *          which is passed into this function.
  *          It's used to compared with a inline
  *          function in this program.
  * @param : s  A Pointer point to A Constant Data Memory
  * @return: INLINE_FUNC_OR_NOT
  */
INLINE_FUNC_OR_NOT nonInlineFunc(const char* s) {
	printf("%s\n", s);

	return __NON_INLINE__;
}

/**
  * @brief : Check operand1 (lesser 100) divisible by
  *          operand2 (lesser 10) using demo in case
  *          fault injection test.
  * @param : operand1  A not negative number lesser 100
  * @param : operand2  A not negative number lesser 10
  * @return: DIVISBLE status
  */
DIVISBLE_StateTypedef
testDivisible(unsigned int operand1, unsigned int operand2) {
	/* parameters' constraints */
	if ( ( operand1 > 99 ) && ( operand2 >  9 || operand2 < 1 ) )
		return __ALL_OPERANDS_OUT_OF_RANGE__;
	else if ( operand1 > 99 )
		return __OPERAND_1_OUT_OF_RANGE__;
	else if ( operand2 >  9 || operand2 < 1 )
		return __OPERAND_2_OUT_OF_RANGE__;

	/* check DIVISBLE or NOT */
	if ( !(operand1 % operand2) )
		return __DIVISBLE__;
	else
		return __NOT_DIVISBLE__;
}

/**
  * @brief : check -Waggregate-return warning
  * @param : st  an AGGREGATE_Typedef variable
  *              using as an in/out parameter
  * @return: none
  */
void passAndRetStructure(AGGREGATE_Typedef& st)
{
	st.a = 1;
	st.b = 2;
}

/**
  * @brief : convert INLINE_FUNC_OR_NOT to unsigned integer
  * @param : param  A INLINE_FUNC_OR_NOT type variable
  * @return: unsigned int
  */
inline unsigned int inlineToUInt(INLINE_FUNC_OR_NOT param) {
	return (unsigned int) param;
}

/**
  * @brief : convert DIVISBLE_StateTypedef to unsigned integer
  * @param : param  A DIVISBLE_StateTypedef type variable
  * @return: unsigned int
  */
inline unsigned int divisibleToUInt(DIVISBLE_StateTypedef param) {
	return (unsigned int) param;
}


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2017 Bao Son Le  ***** END OF FILE ****/
