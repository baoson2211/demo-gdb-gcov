/**
  ******************************************************************************
  * @file    template.h
  * @author  Bao Son Le
  * @version 1.0.2
  * @date    28-Apr-2018
  * @brief   This is template header file.
  ******************************************************************************
  * UPDATE   HISTORY
  * REV      AUTHOR          DATE         DESCRIPTION OF CHANGE
  * ---      -----------     ---------    ---------------------
  * 0.8.0    Bao Son Le      02-Dec-2017  Create this file
  *
  * 1.0.2    Bao Son Le      28-Apr-2018  Edited follow project's template
  *
  ******************************************************************************
  * MIT License
  *
  * Copyright (c) 2017-2018 Bao Son Le
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  *
  ******************************************************************************
  * @attention
  *
  * THIS IS A HEADER TEMPLATE CREATED FOR THE PURPOSE OF PROTOTYPING THE HEADER
  * FILES IN MY LATER PROJECTS. IF YOU HAVE A NEED, YOU CAN CHANGE IT WITHOUT
  * NOTICE.
  *
  * <h2><center>&copy; COPYRIGHT 2017 Bao Son Le </center></h2>
  ******************************************************************************
  */

/* Define to prevent recursive inclusion */
#ifndef __TEMPLATE_H
#define __TEMPLATE_H

/**
  * C++ linkage
  */
#ifdef __cplusplus
extern "C++" {
#endif

/**
  ******************************************************************************
  *  INCLUDES HEADER FILES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED DEFINITIONS - MACROS
  ******************************************************************************
  */
#ifndef OVERWRITTEN_MACRO
#define OVERWRITTEN_MACRO (1000UL)
#endif


/**
  ******************************************************************************
  *  EXPORTED VARIABLES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED STRUCTURES - UNIONS - ENUMERATIONS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED USER-DEFINED TYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED FUNCTION PROTOTYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  CLASSES DECLARATIONS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  NAMESPACES DECLARATIONS
  ******************************************************************************
  */


#ifdef __cplusplus
}
#endif


/******************************************************************************/

/**
  * C linkage
  */
#ifdef __cplusplus
extern "C" {
#endif

/**
  ******************************************************************************
  *  INCLUDES HEADER FILES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED DEFINITIONS - MACROS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED VARIABLES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED STRUCTURES - UNIONS - ENUMERATIONS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED USER-DEFINED TYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED FUNCTION PROTOYPES
  ******************************************************************************
  */


#ifdef __cplusplus
}
#endif

#endif /* __TEMPLATE_H */

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2017 Bao Son Le  ***** END OF FILE ****/
