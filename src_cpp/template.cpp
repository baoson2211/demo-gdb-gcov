/**
  ******************************************************************************
  * @file    template.cpp
  * @author  Bao Son Le
  * @version 0.8.0
  * @date    02-Dec-2017
  * @brief   This is template C++ source file.
  ******************************************************************************
  * UPDATE   HISTORY
  * REV      AUTHOR          DATE         DESCRIPTION OF CHANGE
  * ---      -----------     ---------    ---------------------
  * 0.8.0    Bao Son Le      02-Dec-2017  Create this file
  *
  ******************************************************************************
  * MIT License
  *
  * Copyright (c) 2017-2018 Bao Son Le
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  *
  ******************************************************************************
  * @attention
  *
  * THIS IS A C++ SOURCE TEMPLATE CREATED FOR THE PURPOSE OF PROTOTYPING THE
  * C++ SOURCE FILES IN MY LATER PROJECTS. IF YOU HAVE A NEED, YOU CAN CHANGE
  * IT WITHOUT NOTICE.
  *
  * <h2><center>&copy; COPYRIGHT 2017 Bao Son Le </center></h2>
  ******************************************************************************
  */

/**
  ******************************************************************************
  *  INCLUDES HEADER FILES
  ******************************************************************************
  */
#include "template.h"


/**
  ******************************************************************************
  *  PRIVATE DEFINITIONS - MACROS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE VARIABLES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE STRUCTURES - UNIONS - ENUMERATIONS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE USER-DEFINED TYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE FUNCTION PROTOTYPES
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  EXPORTED FUNCTION DEFINITIONS
  ******************************************************************************
  */


/**
  ******************************************************************************
  *  PRIVATE FUNCTION DEFINITIONS
  ******************************************************************************
  */


/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/******************* (C) COPYRIGHT 2017 Bao Son Le  ***** END OF FILE ****/
