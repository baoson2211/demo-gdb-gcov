##########################################################
# 1. Choose toolchain to be used
# - GCC
# - LLVM
#
TOOLCHAIN   := GCC

# if GCC is chosen
ifeq ($(TOOLCHAIN), GCC)
CC          ?= gcc
CXX         ?= g++
AS          ?= as
AR          ?= ar
COV         ?= gcov
# if LLVM/Clang is chosen
else ifeq ($(TOOLCHAIN), LLVM)
CC          := clang-4.0
CXX         := clang++-4.0
AS          := llvm-as-4.0
AR          := llvm-ar-4.0
COV         := llvm-cov-4.0 gcov
endif

# object copy: utility copies the contents of an object file to another
# object dump: displays information about object files
OBJCOPY     ?= objcopy
OBJDUMP     ?= objdump

# static analysis tool
ANALYST     ?= cppcheck
ANALYOPTS   ?= --enable=all --suppress=missingIncludeSystem --force
ANALYOPTS   += --inconclusive --check-config --std=posix

# profiling tool
GPROF       ?= gprof

# debug tool
GDB         ?= gdb
GDBSCRIPT   ?= gdb.script
GDBOPTS     ?= --command=$(GDBSCRIPT)

# shell commands
TEE         ?= tee
CAT         ?= cat
CAT_V       ?= cat -v
SED_R       ?= sed -r

# regular expression to be used with sed command
REGEX       ?= 's/(\x1B|\^\[)\[+((;*[0-9]{1,2})+?)?[m|K]//g'
REGEX_1     ?= 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2}(;[0-9]{1,2})?)?)?[m|K]//g'
REGEX_2     ?= 's/\^\[//g'
REGEX_3     ?= 's/\[([0-9]{1,2}(;[0-9]{1,2}(;[0-9]{1,2})?)?)?[m|K]//g'

##########################################################
# 2. Enable/Disable generate build log
# - Enable
# - Disable
#
LOG         := Enable
# if enable build log
ifeq ($(LOG), Enable)
LOGGING     ?= 2>&1 | $(TEE) -a $(TEMP_LOG)
endif

# final exec target file
TARGET_EXEC ?= demo.exe

# current working directory
PWD         ?= $(shell pwd)
# choose build directory and build log
BUILD_DIR   ?= build
BUILD_LOG   ?= $(BUILD_DIR)/build.log
# choose temporary directory and temporary log
TEMP_DIR    ?= /tmp
TEMP_LOG    ?= $(TEMP_DIR)/tmp.log

# choose user library directory and source file directory
LIB_DIRS    ?= src_cpp
SRC_DIRS    ?= .

#SRCS := $(shell find $(SRC_DIRS) -iname '*.cpp' -or -iname '*.c' -or -iname '*.s')

# only source codes of library
LIBSRCS     := $(foreach LIB_DIR,$(LIB_DIRS),$(wildcard $(LIB_DIR)/*.cpp))
# all source codes of program
SRCS        := $(foreach SRC_DIR,$(SRC_DIRS),$(wildcard $(SRC_DIR)/*.cpp))
SRCS        += $(foreach LIB_DIR,$(LIB_DIRS),$(wildcard $(LIB_DIR)/*.cpp))
# only object files needed to create shared library
LIBOBJS     := $(LIBSRCS:%.cpp=$(BUILD_DIR)/%.o)
# all object files needed to build program
OBJS        := $(SRCS:%.cpp=$(BUILD_DIR)/%.o)
# dependency info files
DEPS        := $(OBJS:.o=.d)

# directories contain header files
INC_DIRS    := $(shell find $(SRC_DIRS) -type d ! -path "./$(BUILD_DIR)" ! -path "./$(BUILD_DIR)/*" ! -path "./.*" ! -path "./*/.*")
INC_FLAGS   := $(addprefix -I,$(INC_DIRS))


##########################################################
# 3. Use vs. not use shared onject libs
# - NOT_USE_SHARED_LIBRARIES : NOT Use
# - USE_SHARED_LIBRARIES     : Use
#
USELIBS  ?= NOT_USE_SHARED_LIBRARIES

##########################################################
# 4. Coverage
# - CODE_COVERAGE  : build with coverage testDivisible
#
DEFS      = -DCODE_COVERAGE -D$(TOOLCHAIN) $(REDEFINE)

##########################################################
# 5. Debug options
# - Release : compile without debug options
# - Debug   : compile with debug options
#
DEBUG     = Debug

##########################################################
# 6. Redefine macros
#
REDEFINE  = -include overwritten.h

# optimize level
OPTIMIZE  = -O0

# determine the language standard
LANGSTD   = -std=c++14

# compiler's warning options
WARNOPTS  = -Wall -Weffc++ -pedantic \
-pedantic-errors -Wextra -Waggregate-return -Wcast-align \
-Wcast-qual -Wconversion \
-Wdisabled-optimization \
-Wfloat-equal -Wformat=2 \
-Wformat-nonliteral -Wformat-security  \
-Wformat-y2k \
-Wimport -Winit-self -Winline \
-Winvalid-pch \
-Wlong-long \
-Wmissing-field-initializers -Wmissing-format-attribute \
-Wmissing-include-dirs -Wmissing-noreturn \
-Wpacked -Wpadded -Wpointer-arith \
-Wredundant-decls \
-Wshadow -Wstack-protector \
-Wstrict-aliasing=2 -Wswitch-default \
-Wswitch-enum \
-Wunreachable-code -Wunused \
-Wunused-parameter \
-Wvariadic-macros \
-Wwrite-strings

# if debug is enable
ifeq ($(DEBUG), Debug)
CXXFLAGS  = -g -ggdb -gdwarf-2 --coverage -pg
endif

# CXXFLAGS only available if GCC is chosen
ifeq ($(TOOLCHAIN), GCC)
CXXFLAGS += -fstack-usage -ftime-report -fprofile-report
CXXFLAGS += -fdiagnostics-color=always
endif

# CXXFLAGS only available if LLVM/Clang is chosen
ifeq ($(TOOLCHAIN), LLVM)
CXXFLAGS += -stdlib=libstdc++ -ftime-report
CXXFLAGS += -fcolor-diagnostics
endif

# the other option of CXXFLAGS
CXXFLAGS += $(DEFS) $(INC_FLAGS) $(LANGSTD) $(OPTIMIZE) $(WARNOPTS)


# C preprocessor flags
# you can use -MMD option if you don't wanna print system library
# but here, I want it
CPPFLAGS ?= -MD -MP

# C/C++ additional option if you wanna generate assembly files
ASM_GEN  ?= -S -fverbose-asm

# final shared libary target
SHROBJS  ?= libdemo.so
# compiler's linker options
LDFLAGS  ?= -Wl,-Map=$(BUILD_DIR)/$(TARGET_EXEC:.exe=.map) -Wl,--verbose
LDFLAGS  += -lm -lgcc

################## test: check libdemo.so is existed? ####################
# ifeq ($(shell test -e $(BUILD_DIR)/$(SHROBJS) && echo -n yes), yes)
#   LDFLAGS += -L$(BUILD_DIR) -l$(subst .so,,$(subst lib,,$(SHROBJS)))
# endif

# shell commands
MKDIR_P  ?= mkdir -p
CP       ?= cp
LN_SF    ?= ln -sf
