include config.mk

.PHONY: all proj run info lib debug coverage profiling analysis install-lib uninstall-lib clean

all: info proj

proj: $(BUILD_DIR)/$(TARGET_EXEC)

# if use shared libraries
ifeq ($(DIRECTIVE), USE_SHARED_LIBRARIES)
# add share library and the folder which contains the lib
LDFLAGS += -L$(BUILD_DIR) -l$(subst .so,,$(subst lib,,$(SHROBJS)))

$(BUILD_DIR)/$(TARGET_EXEC): lib
	@echo ================================================ $(LOGGING)
	@echo [ linking: build finale executive file $@ ] $(LOGGING)
	@echo $(LOGGING)
	$(CXX) $(CXXFLAGS) main.cpp -o $@ $(LDFLAGS) $(LOGGING)
	$(OBJDUMP) -DSt $@ >$(BUILD_DIR)/$(TARGET_EXEC:.exe=.lst) $(LOGGING)
	@echo $(LOGGING)
	@echo Build finish at $(shell LANG=en_GB date) $(LOGGING)
	@echo [ done.] $(LOGGING)
	@echo $(LOGGING)
# if don't use shared libraries
else
$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	@echo ================================================ $(LOGGING)
	@echo [ linking: build finale executive file $@ ] $(LOGGING)
	@echo $(LOGGING)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $@ $(LDFLAGS) $(LOGGING)
	$(OBJDUMP) -DSt $@ >$(BUILD_DIR)/$(TARGET_EXEC:.exe=.lst) $(LOGGING)
	@echo $(LOGGING)
	@echo Build finish at $(shell LANG=en_GB date) $(LOGGING)
	@echo [ done.] $(LOGGING)
	@echo $(LOGGING)
	$(MAKE) build-log-finalize
	@echo
	$(MAKE) run
endif

# c++ source compile without link
$(BUILD_DIR)/%.o: %.cpp
	@echo ================================================ $(LOGGING)
	@echo [ build without linked file $< ] $(LOGGING)
	@echo $(LOGGING)
	$(MKDIR_P) $(dir $@) $(LOGGING)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -fPIC -c $< -o $@ $(LOGGING)
	$(CXX) $(INC_FLAGS) $(ASM_GEN) $< -o $(BUILD_DIR)/$*.s $(LOGGING)
	$(CPP) $(INC_FLAGS) $(DEFS) $< > $(BUILD_DIR)/$*.ii $(LOGGING)
	@echo $(LOGGING)
	@echo [ done.] $(LOGGING)
	@echo $(LOGGING)

# archive/generate dynamic link library
lib: $(BUILD_DIR)/$(SHROBJS)
$(BUILD_DIR)/$(SHROBJS): $(LIBOBJS)
	@echo ================================================ $(LOGGING)
	@echo [ build $@ from $^ ] $(LOGGING)
	@echo $(LOGGING)
	$(CXX) -shared -o $@ $^ $(LOGGING)
	@echo $(LOGGING)
	@echo [ done.] $(LOGGING)
	@echo $(LOGGING)

# execute
run:
	@echo ================================================
	@echo [ executive ]
	@echo
	$(shell pwd)/$(BUILD_DIR)/$(TARGET_EXEC)
	@echo
	@echo [ done.]
	@echo

# automation test & debugging
debug:
	@echo ================================================
	@echo [ debugging ]
	@echo
	$(GDB) $(GDBOPTS)
	@echo
	@echo [ done.]
	@echo

# code coverage
coverage:
	@echo ================================================
	@echo [ coverage ]
	@echo
# if LLVM/Clang is chosen
ifeq ($(TOOLCHAIN), LLVM)
	$(COV) -bcflnpu $(BUILD_DIR)/main.gcno | c++filt
# if GCC is chosen
else ifeq ($(TOOLCHAIN), GCC)
	$(COV) -bcdfnpu $(BUILD_DIR)/main.gcno | c++filt
endif
	@echo
	@echo [ done.]
	@echo

# profiling
profiling:
	@echo ================================================
	@echo [ profiling ]
	@echo
	$(GPROF) $(BUILD_DIR)/$(TARGET_EXEC)
	@echo
	@echo [ done.]
	@echo

# static code analysis
analysis:
	@echo ================================================
	@echo [ static code analysis ]
	@echo
	$(ANALYST) $(ANALYOPTS) -I /usr/include/c++/5/ $(INC_FLAGS) .
	@echo
	@echo [ done.]
	@echo

# build options/enviroment info
info: build-log-prepare
	@echo ================================================ $(LOGGING)
	@echo [ tool details ] $(LOGGING)
	@echo "Toolchain            : $(TOOLCHAIN)" $(LOGGING)
	@echo "C Compiler           : $(CC)" $(LOGGING)
	@echo "C++ Compiler         : $(CXX)" $(LOGGING)
	@echo "ASM Compiler         : $(AS)" $(LOGGING)
	@echo "C Preprocessor       : $(CPP)" $(LOGGING)
	@echo "Archive maintaining  : $(AR)" $(LOGGING)
	@echo "Permanent Remove     : $(RM)" $(LOGGING)
	@echo "Object Memory Dump   : $(OBJDUMP)" $(LOGGING)
	@echo "Object Copier        : $(OBJCOPY)" $(LOGGING)
	@echo "Make                 : $(MAKE)" $(LOGGING)
	@echo "Debug tool           : $(GDB)" $(LOGGING)
	@echo "Coverage tool        : $(COV)" $(LOGGING)
	@echo "Profile tool         : $(GPROF)" $(LOGGING)
	@echo "Analysis tool        : $(ANALYST)" $(LOGGING)
	@echo "Build configurations : $(DEBUG)" $(LOGGING)
	@echo "Build log config     : $(LOG)" $(LOGGING)
	@echo "Operating System     : $(shell uname -o && uname -m)" $(LOGGING)
	@echo "Machine Hardware     : $(shell uname -m)" $(LOGGING)
	@echo "Kernel release       : $(shell uname -r)" $(LOGGING)
	@echo "Kernel version       : $(shell uname -v)" $(LOGGING)
	@echo $(LOGGING)
	@echo [ project detail ] $(LOGGING)
	@echo "BUILD_DIR            : $(BUILD_DIR)" $(LOGGING)
	@echo "BUILD_LOG            : $(BUILD_LOG)" $(LOGGING)
	@echo "TARGET_EXEC          : $(TARGET_EXEC)" $(LOGGING)
	@echo "INC_DIRS             : $(INC_DIRS)" $(LOGGING)
	@echo "LIB_DIRS             : $(LIB_DIRS)" $(LOGGING)
	@echo "SRC_DIRS             : $(SRC_DIRS)" $(LOGGING)
	@echo "LIBSRCS              : $(LIBSRCS)" $(LOGGING)
	@echo "LIBOBJS              : $(LIBOBJS)" $(LOGGING)
	@echo "SRCS                 : $(SRCS)" $(LOGGING)
	@echo "OBJS                 : $(OBJS)" $(LOGGING)
	@echo "DEPS                 : $(DEPS)" $(LOGGING)
	@echo "GDBSCRIPT            : $(GDBSCRIPT)" $(LOGGING)
	@echo "GDBOPTS              : $(GDBOPTS)" $(LOGGING)
	@echo $(LOGGING)
	@echo [ build flag options ] $(LOGGING)
	@echo "CFLAGS               : $(CFLAGS)" $(LOGGING)
	@echo "CXXFLAGS             : $(CXXFLAGS)" $(LOGGING)
	@echo "CPPFLAGS             : $(CPPFLAGS)" $(LOGGING)
	@echo "LDFLAGS              : $(LDFLAGS)" $(LOGGING)
	@echo "DEFINES              : $(DEFS)" $(LOGGING)
	@echo "INC_FLAGS            : $(INC_FLAGS)" $(LOGGING)
	@echo "ASM GENERATE         : $(ASM_GEN)" $(LOGGING)
	@echo "SHARED LIBS          : $(SHROBJS)" $(LOGGING)
	@echo $(LOGGING)
# in case $(LOG) != "Disable"
ifneq ($(LOG), Disable)
	@echo $(LOGGING) && $(CAT_V) $(TEMP_LOG) | $(SED_R) $(REGEX) > $(BUILD_LOG)
# else $(LOG) == "Disable"
else
	@echo $(LOGGING)
endif

# Finalize build log. You can use 1 in the following choices:
#   $(CAT_V) $(TEMP_LOG) | $(SED_R) $(REGEX) > $(BUILD_LOG)
#   $(CAT) $(TEMP_LOG) | $(SED_R) $(REGEX_1) > $(BUILD_LOG)
#   $(CAT_V) $(TEMP_LOG) | $(SED_R) $(REGEX_2) | $(SED_R) $(REGEX_3) > $(BUILD_LOG)
build-log-finalize:
	@echo ================================================
	@echo [ Finalize build log ]
	@echo
	$(CAT_V) $(TEMP_LOG) | $(SED_R) $(REGEX) > $(BUILD_LOG)
	$(RM) -r $(TEMP_LOG)
	@echo
	@echo [ done.]
	@echo

# Prepare build log in pre-compile phase
build-log-prepare:
# if enable build log
ifneq ($(LOG), Disable)
# if build folder doesn't exist
# create new build folder, build log and remove temp log
# (if it's necessary)
ifeq ("$(wildcard $(BUILD_DIR))", "")
	@echo ================================================
	@echo [ Create output build folder and new build log ]
	@echo
	$(MKDIR_P) $(BUILD_DIR) && echo -n > $(BUILD_LOG) && $(RM) -r $(TEMP_LOG)
	@echo
	@echo [ done.]
	@echo
# else if build folder and build.log existed
# clear build log and remove temp log
# (if it's necessary)
else
	@echo ================================================
	@echo [ Build log is enabled. Regenerate build log ]
	@echo
	echo -n > $(BUILD_LOG) && $(RM) -r $(TEMP_LOG)
	@echo
	@echo [ done.]
	@echo
# endif build folder doesn't exist
endif
# else if disable build log
else
# if build.log existed
# remove build dir and temp log
ifneq ("$(wildcard $(BUILD_LOG))", "")
	@echo ================================================
	@echo [ Build log is disabled. Delete old build log ]
	@echo
	$(RM) -r $(BUILD_LOG) $(TEMP_LOG)
	@echo
	@echo [ done.]
	@echo
endif
# endif enable build log
endif

# clean previuos build
clean:
	$(RM) -r $(BUILD_DIR)
	$(RM) -r $(TEMP_LOG)
	$(RM) -r $(shell find . -type f -iname "*.gcov")
	$(RM) -r $(shell find . -type f -iname "*.log")
	$(RM) -r $(shell find . -type f -iname "*.out")

# install shared lib: symlink or copy: $(CP) $(BUILD_DIR)/$(SHROBJS) /lib
install-lib:
	$(LN_SF) $(PWD)/$(BUILD_DIR)/$(SHROBJS) /lib/$(SHROBJS)

# uninstall shared lib: delete symlink or copy
uninstall-lib:
	$(RM) /lib/$(SHROBJS)

-include $(DEPS)
